<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $users = array(
          ['first_name' => 'Maylyn', 'last_name' => 'Talampas', 'email' => 'maylynstalampas@gmail.com', 'password' => bcrypt('secret'), 'password_confirmation' => bcrypt('secret')],
      );

      foreach ($users as $user) {
          User::create($user);
      }
    }
}
